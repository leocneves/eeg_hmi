#!/usr/bin/env python

import rospkg
import rospy
import os
from os import listdir
from smach import State,StateMachine
import socket
import json
import time

# interval [seconds]
# timeout [seconds]

class GetEmotion(State):
    def __init__(self, interval=1,timeout=300,):
        State.__init__(self, outcomes=['sadness','happiness','neutral','fail'])

        self.data = ''
        self.recovery = ''

    def execute(self, userdata):
        TCP_IP = '127.0.0.1'
        TCP_PORT = 38000
        # BUFFER_SIZE = 20  # Normally 1024, but we want fast response

        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind((TCP_IP, TCP_PORT))
        s.listen(1)

        conn, addr = s.accept()
        print 'Connection address:', addr
        while 1:
            # data = conn.recv(BUFFER_SIZE)
            self.data = conn.recv(20)
            if not self.data: break
            conn.send(self.data)  # echo
            self.recovery = self.data
        conn.close()
        return self.recovery
