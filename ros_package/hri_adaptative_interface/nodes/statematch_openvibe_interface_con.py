#!/usr/bin/env python

import rospy
from hmi_eeg_interface import GetEmotion
from judith_speech import Say, Recognize
import smach
from smach import state
from smach import Concurrence
import smach_ros
import time

def main():
    rospy.init_node('statematch_hmi')
    # creating the concurrence state machine
    sm = Concurrence(outcomes = ['happiness', 'sadness', 'neutral', 'Robot', 'Hello','fail'],
                     default_outcome = 'Robot',
                     outcome_map = {'happiness':{'HUMAN_STATE':'happiness'},
                                                'sadness':{'HUMAN_STATE':'sadness'},
                                                'neutral':{'HUMAN_STATE':'neutral'},
                                                'fail':{'HUMAN_STATE':'fail'},
                                                'Robot':{'RECOGNIZER':'Robot'},
                                                'Hello':{'RECOGNIZER':'Hello'},
                                                'fail':{'RECOGNIZER':'fail'}})
    with sm:

        Concurrence.add('RECOGNIZER', Recognize.Recognize(spec=['Robot','Hello'],time_out=300))

        Concurrence.add('HUMAN_STATE', GetEmotion.GetEmotion())

    # smach.StateMachine.add('CON', sm,
    #     transitions={'fail':'CON','Hello':'Hello','Robot':'Robot','happiness':'happiness','sadness':'sadness','neutral':'neutral',
    #                             })

    sis = smach_ros.IntrospectionServer('hmi_StateMachineServer', sm, '/SM_HMI')
    sis.start()

    # Execute SMACH plan
    outcome = sm.execute()

    rospy.spin()
    sis.stop()


if __name__ == '__main__':
    main()
