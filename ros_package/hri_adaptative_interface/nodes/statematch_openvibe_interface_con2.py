#!/usr/bin/env python

import rospy
from hri_adaptative_interface import GetEmotion
from judith_speech import Say, Recognize
import smach
from smach import state
from smach import Concurrence
import smach_ros
import time
from smach import CBState


# def condition_30_cb(user_data, status):
#     # ... parse JSON in status message ...
#     # return TRUE as long as condition is not met
#     return status.simulationTime < 30


# @smach.cb_interface(input_keys=[], output_keys=[], outcomes=['finished'])
# def action_30_cb(user_data):
#     # ... do something
#     return 'finished'


# def condition_spike_cb(user_data, spike):
#     # ... parse JSON in spike message ...
#     # return TRUE as long as condition is not met
#     return spike < 10


# @smach.cb_interface(input_keys=[], output_keys=[], outcomes=['finished'])
# def action_spike_cb(user_data):
#     # ... do something
#     return 'finished'

if __name__ == "__main__":
    # Create a ROS node for this state machine
    rospy.init_node("hmi_control")

    sm_one = smach.StateMachine(
    outcomes=['HELLO','FOLLOW','SORRY'])

    with sm_one:
        smach.StateMachine.add('RECOGNIZE', Recognize.Recognize(spec=['Robot','Follow me'],time_out=300),
                               transitions={'Robot':'HELLO','Follow me':'FOLLOW','fail':'SORRY'})


    sm_two = smach.StateMachine(
    outcomes=['H','S','N','F'])

    with sm_two:
        smach.StateMachine.add('HUMAN_EMOTION', GetEmotion.GetEmotion(),
                               transitions={'happiness': 'H','sadness':'S','neutral':'N','fail':'F'})


    sm = smach.Concurrence(outcomes=['R','H','S','N','F'],
                           default_outcome='F', input_keys=[], output_keys=[],
                           outcome_map = {'R':{'SM_ONE':'HELLO'},
                                     'R':{'SM_ONE':'FOLLOW'},
                                     'R':{'SM_ONE':'SORRY'},
                                     'H':{'SM_TWO':'H'},
                                     'S':{'SM_TWO':'S'},
                                     'N':{'SM_TWO':'N'},
                                     'F':{'SM_TWO':'F'}})

    with sm:
        smach.Concurrence.add('SM_ONE', sm_one)
        smach.Concurrence.add('SM_TWO', sm_two)

    sis = smach_ros.IntrospectionServer('judith_StateMachineServer', sm, '/SM_JUDITH')
    sis.start()

    result = sm.execute()
    # rospy.loginfo(result)

    rospy.spin()
    sis.stop()
