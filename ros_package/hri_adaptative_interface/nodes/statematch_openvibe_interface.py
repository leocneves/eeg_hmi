#!/usr/bin/env python

import rospy
from hri_adaptative_interface import GetEmotion
import smach
from smach import state, Concurrence
import smach_ros
import time

def main():
    rospy.init_node('statematch_hmi')

    # Create a SMACH state machine
    sm = smach.StateMachine(outcomes=['success1','fail'])


    cc = Concurrence(outcomes = ['outcome1', 'outcome2'],
                     default_outcome = 'outcome1',
                     input_keys = ['sm_input'],
                     output_keys = ['sm_output'],
                     outcome_map = {'happiness':{'EMOTION_STATE':'happiness'},
                                                'sadness':{'EMOTION_STATE':'sadness'},
                                                'neutral':{'EMOTION_STATE':'neutral'},
                                                'outcome3':{'FOO':'outcome2'}})
    with cc:
        Concurrence.add('EMOTION_STATE', GetEmotion.GetEmotion())
        Concurrence.add(sm)
        with sm:
            # Add states to the container
            smach.StateMachine.add('GET_STATE', GetEmotion.GetEmotion(),
                                   transitions={'sadness':'success1','happiness':'success2','neutral':'success3','fail':'outcome4'})


    sis = smach_ros.IntrospectionServer('hmi_StateMachineServer', sm, '/SM_HMI')
    sis.start()

    # Execute SMACH plan
    outcome = sm.execute()

    rospy.spin()
    sis.stop()


if __name__ == '__main__':
    main()
